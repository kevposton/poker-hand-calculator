﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class Straight : IHandRank
    {
        private const Int32 HIGH_CARD = 14;
        private const Int32 LOW_CARD = 2;
        private readonly IEnumerable<ICard> cards;

        public Straight(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Any() == false)
                return false;

            if (cards.Select(c => c.Rank).Distinct().Count() < cards.Count())
                return false;

            return IsStraight();
        }

        private Boolean IsStraight()
        {
            var sortedCards = cards.Select(c => c.Rank).OrderBy(c => c);

            if (IsDefaultStraight(sortedCards))
                return true;

            var cardsContainAce = sortedCards.Any(c => c == HIGH_CARD);

            if (cardsContainAce)
                return IsAceLowStraight(sortedCards);

            return false;
        }

        private Boolean IsDefaultStraight(IOrderedEnumerable<Int32> orderedCards)
        {
            var lastCard = orderedCards.Last();
            var firstCard = orderedCards.First();

            return firstCard + orderedCards.Count() - 1 == lastCard;
        }

        private Boolean IsAceLowStraight(IOrderedEnumerable<Int32> orderedCards)
        {
            var firstCard = orderedCards.First();
			
            if (firstCard == LOW_CARD)
            {
                var cardsWithoutAce = orderedCards.Where(c => c != HIGH_CARD).OrderBy(c => c);

                return IsDefaultStraight(cardsWithoutAce);
            }

            return false;
        }

        public String GetName()
        {
            return "STRAIGHT";
        }
    }
}
