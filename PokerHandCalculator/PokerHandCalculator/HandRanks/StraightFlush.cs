﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class StraightFlush : IHandRank
    {
        private readonly IEnumerable<ICard> cards;

        public StraightFlush(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Any() == false)
                return false;

            var flush = new Flush(cards);
            var straight = new Straight(cards);

            return flush.Exists() && straight.Exists();
        }

        public String GetName()
        {
            return "STRAIGHT FLUSH";
        }
    }
}
