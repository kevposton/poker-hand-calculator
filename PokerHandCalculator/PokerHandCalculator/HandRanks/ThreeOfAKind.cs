﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class ThreeOfAKind : IHandRank
    {
        private const Int32 NUMBER_OF_CARDS_NEEDED = 3;
        private readonly IEnumerable<ICard> cards;

        public ThreeOfAKind(IEnumerable<ICard> cards) 
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Count() < NUMBER_OF_CARDS_NEEDED)
                return false;

            return cards.GroupBy(c => c.Rank).Any(g => g.Count() > 2);
        }

        public String GetName()
        {
            return "THREE OF A KIND";
        }
    }
}
