﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.HandRanks
{
    public class Flush : IHandRank
    {
        private readonly IEnumerable<ICard> cards;

        public Flush(IEnumerable<ICard> cards)
        {
            this.cards = cards;
        }

        public Boolean Exists()
        {
            if (cards.Any() == false)
                return false;

            return cards.Select(c => c.Suit).Distinct().Count() == 1;
        }

        public String GetName()
        {
            return "FLUSH";
        }
    }
}
