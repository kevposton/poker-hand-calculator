﻿using System;

namespace PokerHandCalculator.HandRanks
{
    public interface IHandRank
    {
        Boolean Exists();
        String GetName();
    }
}
