﻿using System.Collections.Generic;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.Hands
{
    public interface IHandFactory
    {
        IHand CreatePokerHand(IEnumerable<ICard> cards);
        IHand CreateThreeCardPokerHand(IEnumerable<ICard> cards);
    }
}
