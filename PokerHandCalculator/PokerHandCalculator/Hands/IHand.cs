﻿using System;

namespace PokerHandCalculator.Hands
{
    public interface IHand
    {
        Int32 NumberOfCards { get; }
        String Calculate();
    }
}