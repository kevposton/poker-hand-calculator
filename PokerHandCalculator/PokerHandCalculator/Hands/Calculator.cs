﻿using System;
using System.Collections.Generic;
using PokerHandCalculator.Cards;

namespace PokerHandCalculator.Hands
{
    public class Calculator
    {
        private readonly ICardRepository cardRepository;
        private readonly IHandFactory handFactory;

        public Calculator(ICardRepository cardRepository, IHandFactory handFactory)
        {
            this.cardRepository = cardRepository;
            this.handFactory = handFactory;
        }

        public String CalculatePokerHand(String input)
        {
            var cards = GetCards(input);
            var pokerHand = handFactory.CreatePokerHand(cards);

            return pokerHand.Calculate();
        }

        public String CalculateThreeCardPokerHand(String input)
        {
            var cards = GetCards(input);
            var pokerHand = handFactory.CreateThreeCardPokerHand(cards);

            return pokerHand.Calculate();
        }

        private IEnumerable<ICard> GetCards(String input)
        {
            var cardCodes = input.Split(' ');
            var cards = new List<ICard>();

            foreach (var cardCode in cardCodes)
            {
                var card = cardRepository.Find(cardCode);

                if (card == null)
                    throw new Exception(String.Format("{0} is not a valid poker card.", cardCode));

                cards.Add(card);
            }

            return cards;
        }
    }
}
