﻿using System;

namespace PokerHandCalculator.Cards
{
    public interface ICardRepository
    {
        ICard Find(String code);
    }
}
