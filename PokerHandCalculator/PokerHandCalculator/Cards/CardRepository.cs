﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PokerHandCalculator.Cards
{
    public class CardRepository : ICardRepository
    {
        private readonly IEnumerable<ICard> cards;

        public CardRepository()
        {
            cards = new[] {
                new Card("2C", 2, "CLUBS", "TWO"),
                new Card("3C", 3, "CLUBS", "THREE"),
                new Card("4C", 4, "CLUBS", "FOUR"),
                new Card("5C", 5, "CLUBS", "FIVE"),
                new Card("6C", 6, "CLUBS", "SIX"),
                new Card("7C", 7, "CLUBS", "SEVEN"),
                new Card("8C", 8, "CLUBS", "EIGHT"),
                new Card("9C", 9, "CLUBS", "NINE"),
                new Card("10C", 10, "CLUBS", "TEN"),
                new Card("JC", 11, "CLUBS", "JACK"),
                new Card("QC", 12, "CLUBS", "QUEEN"),
                new Card("KC", 13, "CLUBS", "KING"),
                new Card("AC", 14, "CLUBS", "ACE"),
                new Card("2D", 2, "DIAMONDS", "TWO"),
                new Card("3D", 3, "DIAMONDS", "THREE"),
                new Card("4D", 4, "DIAMONDS", "FOUR"),
                new Card("5D", 5, "DIAMONDS", "FIVE"),
                new Card("6D", 6, "DIAMONDS", "SIX"),
                new Card("7D", 7, "DIAMONDS", "SEVEN"),
                new Card("8D", 8, "DIAMONDS", "EIGHT"),
                new Card("9D", 9, "DIAMONDS", "NINE"),
                new Card("10D", 10, "DIAMONDS", "TEN"),
                new Card("JD", 11, "DIAMONDS", "JACK"),
                new Card("QD", 12, "DIAMONDS", "QUEEN"),
                new Card("KD", 13, "DIAMONDS", "KING"),
                new Card("AD", 14, "DIAMONDS", "ACE"),
                new Card("2H", 2, "HEARTS", "TWO"),
                new Card("3H", 3, "HEARTS", "THREE"),
                new Card("4H", 4, "HEARTS", "FOUR"),
                new Card("5H", 5, "HEARTS", "FIVE"),
                new Card("6H", 6, "HEARTS", "SIX"),
                new Card("7H", 7, "HEARTS", "SEVEN"),
                new Card("8H", 8, "HEARTS", "EIGHT"),
                new Card("9H", 9, "HEARTS", "NINE"),
                new Card("10H", 10, "HEARTS", "TEN"),
                new Card("JH", 11, "HEARTS", "JACK"),
                new Card("QH", 12, "HEARTS", "QUEEN"),
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("AH", 14, "HEARTS", "ACE"),
                new Card("2S", 2, "SPADES", "TWO"),
                new Card("3S", 3, "SPADES", "THREE"),
                new Card("4S", 4, "SPADES", "FOUR"),
                new Card("5S", 5, "SPADES", "FIVE"),
                new Card("6S", 6, "SPADES", "SIX"),
                new Card("7S", 7, "SPADES", "SEVEN"),
                new Card("8S", 8, "SPADES", "EIGHT"),
                new Card("9S", 9, "SPADES", "NINE"),
                new Card("10S", 10, "SPADES", "TEN"),
                new Card("JS", 11, "SPADES", "JACK"),
                new Card("QS", 12, "SPADES", "QUEEN"),
                new Card("KS", 13, "SPADES", "KING"),
                new Card("AS", 14, "SPADES", "ACE")
            };
        }

        public ICard Find(String code)
        {
            return cards.FirstOrDefault(c => c.Code == code);
        }
    }
}
