﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PokerHandCalculator.Cards;
using PokerHandCalculator.Hands;

namespace PokerHandCalculatorTests.Unit.Hands
{
    [TestClass]
    public class CalculatorTests
    {
        private Mock<ICardRepository> mockCardRepository;
        private Mock<IHandFactory> mockHandFactory;
        private Calculator handCalculator;

        public CalculatorTests()
        {
            mockCardRepository = new Mock<ICardRepository>();
            mockHandFactory = new Mock<IHandFactory>();
            handCalculator = new Calculator(mockCardRepository.Object, mockHandFactory.Object);
        }

        [TestMethod]
        public void CalculatePokerHandThrowsExceptionWhenCardCannotBeFound()
        {
            var invalidCardCode = "ZZ";
            var expectedExceptionMessage = String.Format("{0} is not a valid poker card.", invalidCardCode);

            var actualException = Assert.ThrowsException<Exception>(
                () => handCalculator.CalculatePokerHand(invalidCardCode));

            Assert.AreEqual(expectedExceptionMessage, actualException.Message);
        }

        [TestMethod]
        public void CalculateThreePokerHandThrowsExceptionWhenCardCannotBeFound()
        {
            var invalidCardCode = "ZZ";
            var expectedExceptionMessage = String.Format("{0} is not a valid poker card.", invalidCardCode);

            var actualException = Assert.ThrowsException<Exception>(
                () => handCalculator.CalculateThreeCardPokerHand(invalidCardCode));

            Assert.AreEqual(expectedExceptionMessage, actualException.Message);
        }

        [TestMethod]
        public void CalculatePokerHandReturnsCalculationOfPokerHandCreatedByFactory()
        {
            var cardCode = "ZZ";
            var mockCard = new Mock<ICard>();
            var mockHand = new Mock<IHand>();
            var expectedResult = "Calculated";
            mockHand.Setup(h => h.Calculate()).Returns(expectedResult);
            mockCardRepository.Setup(r => r.Find(cardCode)).Returns(mockCard.Object);
            mockHandFactory.Setup(f => f.CreatePokerHand(new[] { mockCard.Object })).Returns(mockHand.Object);

            var result = handCalculator.CalculatePokerHand(cardCode);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void CalculateThreeCardPokerHandReturnsCalculationOfPokerHandCreatedByFactory()
        {
            var cardCode = "ZZ";
            var mockCard = new Mock<ICard>();
            var mockHand = new Mock<IHand>();
            var expectedResult = "Calculated";
            mockHand.Setup(h => h.Calculate()).Returns(expectedResult);
            mockCardRepository.Setup(r => r.Find(cardCode)).Returns(mockCard.Object);
            mockHandFactory.Setup(f => f.CreateThreeCardPokerHand(new[] { mockCard.Object })).Returns(mockHand.Object);

            var result = handCalculator.CalculateThreeCardPokerHand(cardCode);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
