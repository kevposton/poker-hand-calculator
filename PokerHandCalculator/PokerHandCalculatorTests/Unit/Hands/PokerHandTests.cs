﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;
using PokerHandCalculator.Hands;

namespace PokerHandCalculatorTests.Unit.Hands
{
    [TestClass]
    public class PokerHandTests
    {
        [TestMethod]
        public void CalculateThrowsExceptionWhenHandDoesNotHaveCorrectNumberOfCards()
        {
            var numberOfCards = 5;
            var expectedExceptionMessage = String.Format("Must have {0} cards for a valid hand.", numberOfCards);
            var cards = Enumerable.Empty<ICard>();
            var handRanks = new List<IHandRank>();
            var pokerHand = new PokerHand(numberOfCards, cards, handRanks);

            var thrownException = Assert.ThrowsException<Exception>(() => pokerHand.Calculate());

            Assert.AreEqual(expectedExceptionMessage, thrownException.Message);
        }

        [TestMethod]
        public void CalculateThrowsExceptionWhenHandHasDuplicateCards()
        {
            var expectedExceptionMessage = "Duplicate cards are not valid for poker hand.";
            var handRanks = new List<IHandRank>();
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("AH", 14, "HEARTS", "ACE"),
                new Card("QH", 12, "HEARTS", "QUEEN"),
                new Card("JH", 11, "HEARTS", "JACK")
            };
            var hand = new PokerHand(5, cards, handRanks);

            var actualException = Assert.ThrowsException<Exception>(() => hand.Calculate());

            Assert.AreEqual(expectedExceptionMessage, actualException.Message);
        }

        [TestMethod]
        public void CalculateReturnsHighCardWhenNoOtherHandRanksExist()
        {
            var mockHandRank = new Mock<IHandRank>();
            mockHandRank.Setup(r => r.Exists()).Returns(false);
            var handRanks = new List<IHandRank> { mockHandRank.Object };
            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING"),
                new Card("AH", 14, "HEARTS", "ACE"),
                new Card("QH", 12, "HEARTS", "QUEEN")
            };
            var pokerHand = new PokerHand(3, cards, handRanks);

            var rank = pokerHand.Calculate();

            Assert.AreEqual("ACE HIGH", rank);
        }

        [TestMethod]
        public void CalculateReturnsRankNameThatExists()
        {
            var expectedRank = "EXPECTED";
            var mockExpectedHandRank = new Mock<IHandRank>();
            var mockHandRank = new Mock<IHandRank>();
            mockExpectedHandRank.Setup(r => r.Exists()).Returns(true);
            mockExpectedHandRank.Setup(r => r.GetName()).Returns(expectedRank);
            mockHandRank.Setup(r => r.Exists()).Returns(false);
            mockHandRank.Setup(r => r.GetName()).Returns("ERROR");

            var cards = new[]
            {
                new Card("KH", 13, "HEARTS", "KING")
            };
            var handRanks = new List<IHandRank>
            {
                mockHandRank.Object,
                mockExpectedHandRank.Object
            };
            var pokerHand = new PokerHand(1, cards, handRanks);

            var rank = pokerHand.Calculate();

            Assert.AreEqual(expectedRank, rank);
        }
    }
}
