﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.HandRanks;

namespace PokerHandCalculatorTests.Unit.HandRanks
{
    [TestClass]
    public class PairTests
    {
		[TestMethod]
		public void GetNameReturnsPair()
		{
			var pair = new Pair(Enumerable.Empty<ICard>());
			
			Assert.AreEqual("PAIR", pair.GetName());
		}

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedNoCards()
        {
            var pair = new Pair(Enumerable.Empty<ICard>());

            Assert.IsFalse(pair.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseWhenPassedOneCard()
        {
            var pair = new Pair(new[] { new Card("5H", 5, "HEARTS", "FIVE") });

            Assert.IsFalse(pair.Exists());
        }

        [TestMethod]
        public void ExistsReturnsTrueForTwoFives()
        {
            var cards = new[] 
            { 
                new Card("5H", 5, "HEARTS", "FIVE"), 
                new Card("5S", 5, "SPADES", "FIVE") 
            };

            var pair = new Pair(cards);

            Assert.IsTrue(pair.Exists());
        }

        [TestMethod]
        public void ExistsReturnsFalseForFiveAndTen()
        {
            var cards = new[] 
            { 
                new Card("5H", 5, "HEARTS", "FIVE"), 
                new Card("10S", 10, "SPADES", "TEN") 
            };

            var pair = new Pair(cards);

            Assert.IsFalse(pair.Exists());
        }
    }
}
