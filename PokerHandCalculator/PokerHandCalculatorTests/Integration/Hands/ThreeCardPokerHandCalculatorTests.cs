﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerHandCalculator.Cards;
using PokerHandCalculator.Hands;

namespace PokerHandCalculatorTests.Integration.Hands
{
    [TestClass]
    public class ThreeCardPokerHandCalculatorTests
    {
        private ICardRepository cardRepository;
        private IHandFactory handFactory;
        private Calculator calculator;

        public ThreeCardPokerHandCalculatorTests()
        {
            this.cardRepository = new CardRepository();
            this.handFactory = new PokerHandFactory();
            this.calculator = new Calculator(cardRepository, handFactory);
        }

        [TestMethod]
        public void CalculateReturnsJackHighForJackHighCards()
        {
            Assert.AreEqual("JACK HIGH", calculator.CalculateThreeCardPokerHand("JS 10D 8H"));
        }

        [TestMethod]
        public void CalculateReturnsPairForCardsWithAPair()
        {
            Assert.AreEqual("PAIR", calculator.CalculateThreeCardPokerHand("JS JD 4H"));
        }

		[TestMethod]
		public void CalculateReturnsFlushForCardsWithSameSuit()
		{
            Assert.AreEqual("FLUSH", calculator.CalculateThreeCardPokerHand("7S 4S 9S"));
		}

		[TestMethod]
		public void CalculateReturnsStraightForCardsWithSequentialRanks()
		{
            Assert.AreEqual("STRAIGHT", calculator.CalculateThreeCardPokerHand("7S 8S 9H"));
		}

        [TestMethod]
        public void CalculateReturnsThreeOfAKindForCardsWithThreeOfAKind()
        {
            Assert.AreEqual("THREE OF A KIND", calculator.CalculateThreeCardPokerHand("JS JC JH"));
        }

        [TestMethod]
        public void CalculateReturnsStraightFlushWhenCardsHaveSameSuitAndAreSequenced()
        {
            Assert.AreEqual("STRAIGHT FLUSH", calculator.CalculateThreeCardPokerHand("7S 8S 9S"));
        }

        [TestMethod]
        public void CalculateReturnsRoyalFlushWhenCardsHaveSameSuitAndAreSequencedAceHigh()
        {
            Assert.AreEqual("ROYAL FLUSH", calculator.CalculateThreeCardPokerHand("AS KS QS"));
        }
    }
}
